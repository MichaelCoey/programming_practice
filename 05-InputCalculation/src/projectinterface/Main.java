package projectinterface;

import java.util.Scanner;

public class Main {
	
	/* Program that will ask for the user to input two numbers and then will
	 * perform various calculations on the inputed numbers and output the results
	 * 
	 *  Test Data
	 *  Input first number: 125
	 *  Input second number: 24
	 *  
	 *  Expected Result
	 *  125 + 24 = 149
	 *  125 - 24 = 101
	 *  125 x 24 = 3000
	 *  125 / 24 = 5
	 *  125 mod 24 = 5
	 *  
	 *  Numbers will vary depending on the input but these numbers are known
	 *  */

	public static void main(String[] args) {
		
		//Variables to hold the users input
		int numberOne;
		int numberTwo;
		
		//Scanner to read the user inputs
		Scanner sc = new Scanner(System.in);
		
		//Asking for the users inputs and reading in the values
		System.out.println("Please enter the first number");
		numberOne = sc.nextInt();
		
		System.out.println("Please enter the second number");
		numberTwo = sc.nextInt();
		
		//Performing the calculations and displaying
		int temp; //Temp variable to store results in
		
		temp = numberOne + numberTwo;
		System.out.println(numberOne + " + " + numberTwo + " = " + temp);
		
		temp = numberOne - numberTwo;
		System.out.println(numberOne + " - " + numberTwo + " = " + temp);
		
		temp = numberOne * numberTwo;
		System.out.println(numberOne + " x " + numberTwo + " = " + temp);
		
		temp = numberOne / numberTwo;
		System.out.println(numberOne + " / " + numberTwo + " = " + temp);
		
		temp = numberOne % numberTwo;
		System.out.println(numberOne + " mod " + numberTwo + " = " + temp);
		
		sc.close();
	}

}
