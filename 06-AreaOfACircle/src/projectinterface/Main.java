package projectinterface;

import java.text.DecimalFormat;

public class Main {
	
	/* This program will print the area and circumference of a circle using the following
	 * 
	 *  Test Data
	 *  Radius - 7.5
	 *  
	 *  Expected Result
	 *  Radius is 7.5
	 *  Circumference is 47.12388980384689
	 *  Area is 176.71458676442586
	 *  
	 *  These results should be shown in two decimal places, I will use a decimal format for this
	 *  
	 *  */
	
	//Decimal format to 2 decimal places
	private static DecimalFormat df2 = new DecimalFormat(".##");

	public static void main(String[] args) {
		
		//Variables needed to store results
		double radius = 7.5;
		double circumference;
		double area;
		
		//Calculations
		
		circumference = 2 * Math.PI * radius;
		area = Math.PI * radius * radius;
		
		//Output
		System.out.println("Radius = " + radius);
		System.out.println("Circumference = " + df2.format(circumference));
		System.out.println("Area = " + df2.format(area));

	}

}
