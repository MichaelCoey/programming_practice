package projectinterface;

public class Main {
	
	/* In this program I will be writing multiple operations for the program to work out
	 * these will be mathematical problems and the test data is below
	 * 
	 *  Test Data
	 *  -5 + 8 * 6
	 *  (55 + 9) % 9
	 *  20 + -3 * 5 / 8
	 *  5 + 15 / 3 * 2 - 8 % 3
	 *  
	 *  Expected Output
	 *  43
	 *  1
	 *  19
	 *  13
	 *  */

	public static void main(String[] args) {

		//Variables to hold data
		int numberOne;
		int numberTwo;
		int numberThree;
		int numberFour;
		
		//Performing the calculations
		numberOne = -5 + 8 * 6; //43
		numberTwo = (55 + 9) % 9; //1
		numberThree = 20 + -3 * 5 / 8; //19
		numberFour = 5 + 15 / 3 * 2 - 8 % 3; //13
		
		//Displaying the calculations
		System.out.println(numberOne); //Correct
		System.out.println(numberTwo); //Correct
		System.out.println(numberThree); //Correct
		System.out.println(numberFour); //Correct
		
	}

}
