package projectinterface;

import java.util.Scanner;

public class Main {
	
	/* This program will output the area and the perimeter of a rectangle based 
	 * on the users input */
	
	/*
	 * ==Test Data==
	 * Width = 5.6
	 * Height = 8.5
	 * 
	 * ==Expected Output==
	 * Area is 5.6 x 8.5 = 47.60
	 * Perimeter is 2 * (5.6 + 8.5) = 28.2
	 *  */

	public static void main(String[] args) {
		
		//Variables needed for the calculation
		float width;
		float height;
		float area;
		float perimeter;
		
		//Scanner to read user inputs
		Scanner sc = new Scanner(System.in);
		
		//Asking users for the required inputs
		System.out.println("Please enter the width below:");
		width = sc.nextFloat();
		
		System.out.println("Please enter the height below:");
		height = sc.nextFloat();
		
		//Calculation
		area = width * height;
		perimeter = 2 * (width + height);
		
		//Output the results
		System.out.println("Area is " + width + " x " + height + " = " + area);
		System.out.println("Perimeter is 2 x (" + width + " + " + height + ") = " + perimeter );
		
		//Closing the scanner to stop resource leaks
		sc.close();
		

	}

}
