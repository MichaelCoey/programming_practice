package projectinterface;

import java.util.Scanner;

public class Main {
	
	/* This program will be similar to the previous example except this time
	 * the program will check for a palindrome after converting to lowercase
	 * 
	 * == Test Data ==
	 * Input = Dad
	 * Output = This word is a palindrome
	 * 
	 * Input = Happy
	 * Output = This word is not a palindrome
	 *  */

	public static void main(String[] args) {
		
		//Variables to store the input and the reverse word in
		String input;
		String reverse = "";
		
		//Scanner to read user inputs
		Scanner sc = new Scanner(System.in);
		
		//Asking the user for the word they want to check
		System.out.println("Please enter a word you wish to check for a palindrome");
		
		//Reading the input and converting to lower case
		input = sc.nextLine().toLowerCase();
		
		//Reversing the original input
		for(int i = input.length() - 1; i >= 0; i--) {
			
			reverse = reverse + input.charAt(i);
		
		}
		
		//Checking the two words to see if the input is a palindrome
		System.out.println("==== Results ====");
		if(reverse.equals(input)) {
			System.out.println("The input entered was " + input);
			System.out.println("The word reversed was " + reverse);
			System.out.println(input + " is a palindrome");	
		}
		else {
			System.out.println("The input entered was " + input);
			System.out.println("The word reversed was " + reverse);
			System.out.println(input + " is not a palindrome");
		}
		
		//Closing the scanner to stop resource leaks
		sc.close();

	}

}
