package projectinterface;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
	
	//Decimal format to output result to two decimal places
	//Placed outside main as it is a final object and can be used in any method in this class
	private static DecimalFormat roundTotalAnswer = new DecimalFormat(".##");

	public static void main(String[] args) {
		
		//* Simple program that will read two numbers the user inputs and divide them *\\
		
		
		
		//Variables to store the numbers
		double numberOne;
		double numberTwo;
		double totalNumber;
		
		//Scanner to read the user inputs
		Scanner sc = new Scanner(System.in);
		
		//Reading the first number the user enters
		System.out.println("Please enter the first number below:");
		numberOne = sc.nextDouble();
		
		//Reading the second number the user enters
		System.out.println("Please enter the second number below:");
		numberTwo = sc.nextDouble();
		
		//Performing the division on the entered numbers
		totalNumber = numberOne / numberTwo;
		
		//Displaying the results to the user
		System.out.println("The result of the division is the following:");
		//Output rounded to two decimal places using the decimal format.format method
		System.out.println(numberOne + " / " + numberTwo + " = " + roundTotalAnswer.format(totalNumber));
		
		//Closing the scanner to stop resource leaks
		sc.close();

	}

}
