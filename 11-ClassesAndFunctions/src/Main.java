
public class Main {

	/* This program was created to have several programs within to showcase different classes
	 * and functions being called. The different functions that this 
	 * program will do are as follows.
	 * 
	 *  == Program Function ==
	 *  1. Caravan booking service
	 *  2. Discount on multiple purchases
	 *  3. Delivery charge and distance
	 *  4. Traffic light display simulation
	 *  5. Password checker
	 *  6. Car lights, on or off
	 *  7. Take money from an ATM
	 *  
	 *  These will all be individual classes and the functions
	 *  will be called depending on the users choice.
	 *  
	 *  This class is the main class and will run when the program is started.
	 *  
	 *  */
	
	public static void main(String[] args) {
		
		//When the program launches this will call the main menu from the project interface class.
		//From this main menu the choices will be made to run what the user wants.
		projectinterface.MainMenu.MainMenuInterface();

	}

}
