package projectinterface;

import java.util.Scanner;

public class MainMenu {
	
	/* This class will prompt the user for a number, depending on the input the relating
	 * function will run, */
	
	public static void MainMenuInterface() {
		
		//Variable to store the users input
		int menuChoice = 0;
		
		//Scanner to read the user input
		Scanner sc = new Scanner(System.in);
		
		//Instructions to the user
		System.out.println("== Main Menu ==");
		System.out.println("---------------");
		System.out.println("Please enter a choice below (1-7):");
		System.out.println("1. Caravan Park Rental Charges");
		System.out.println("2. Multibuy Discount");
		System.out.println("3. Delivery Charges");
		System.out.println("4. Traffic Light Simulation");
		System.out.println("5. Password Checker");
		System.out.println("6. Car Light Checker");
		System.out.println("7. Cash withdrawal");
		
		//Reading the users input
		menuChoice = sc.nextInt();
		
		//Depending on the users input the relating method will be called
		if(menuChoice == 1) {
			projectclasses.CaravanRental.caravanCharges();
		}
		else if(menuChoice == 2) {
			projectclasses.MultibuyDiscount.discountAmount();
		}
		else if(menuChoice == 3) {
			projectclasses.Delivery.deliveryCharge();
		}
		else if(menuChoice == 4) {
			
		}
		else if(menuChoice == 5) {
			
		}
		else if(menuChoice == 6) {
			
		}
		else if(menuChoice == 7) {
			
		}
		else {
			
		}
		
		//Closing scanner to stop resource leak
		sc.close();
		
	}

}
