package projectclasses;

import java.util.Scanner;

public class MultibuyDiscount {
	
	/* This class will perform a basic check to see how much the user is wanting to buy
	 * depending on the quantity a discount will be available. The amount of discount the user
	 * will receive will be displayed to the user along with the quantity they wish to buy.
	 * 
	 *  == Discount Amounts ==
	 *  Less than 10 = 5% discount
	 *  Less than 50 = 10% discount
	 *  Less than 100 = 15% discount
	 *  Over 100 = 20% discount
	 *  
	 *  */
	
	public static void discountAmount() {
		
		//Variables to store number of products and menuChoice
		int productAmount;
		String menuChoice;
		
		//Scanner to read input
		Scanner sc = new Scanner(System.in);
		
		//Reading the user input
		System.out.println("How many products are you wanting to buy?");
		System.out.println("Please enter an amount below: (e.g 15)");
		productAmount = sc.nextInt();
		
		//Checking the amount entered and displaying the correct discount
		if(productAmount < 0) {			
			System.out.println("The requested amount was " + productAmount + ". This is not a valid option, please try again.");
			discountAmount();
		}
		else if(productAmount == 0) {			
			System.out.println("The requested amount was " + productAmount + ". This will get you 0% discount.");
		}
		else if(productAmount > 0 && productAmount < 10) {			
			System.out.println("The requested amount was " + productAmount + ". This will get you 5% discount.");
		}
		else if(productAmount < 50) {			
			System.out.println("The requested amount was " + productAmount + ". This will get you 10% discount.");
		}
		else if(productAmount < 100) {			
			System.out.println("The requested amount was " + productAmount + ". This will get you 15% discount.");
		}
		else if(productAmount >= 100) {
			System.out.println("The requested amount was " + productAmount + ". This will get you 20% discount.");
		}
		else {
			System.out.println("This is not a vaild option, please try again.");
			discountAmount();
		}
		
		
		//Option to close program or return to main menu
		System.out.println("Would you like to return to the main menu? (Yes/No)");
		menuChoice = sc.next();
		
		if(menuChoice.toLowerCase().startsWith("y")) {
			
			projectinterface.MainMenu.MainMenuInterface();
			
		}
		else if(menuChoice.toLowerCase().startsWith("n")) {
			
			System.out.println("Thank you, the program is now closing");
			System.exit(0);
			
		}
		else {
			
			System.out.println("I am sorry, that is not a valid option, please try again");
			discountAmount();
			
		}
		
		
		sc.close();
		
	}

}
