package projectclasses;

import java.util.Scanner;

public class CaravanRental {
	
	/* A caravan park charges different rates per night depending on whether or not the caravan
	 * owner is a member, i.e owns a spot in the park, or not. It also bases its charges on the
	 * length of stay.
	 * 
	 *  Charges are as follows:
	 *  Member - 7 nights or less �12.99 per night
	 *  Member - More than 7 nights �9.99 per night
	 *  Non-Member - 7 nights or less �14.99 per night
	 *  Non-Member - More than 7 nights �11.99 per night
	 *  
	 *  */
	
	public static void caravanCharges() {
		
		//Class variables
		String memberInput = "";
		String menuChoice = "";
		boolean memberStatus = false;
		int lengthOfStay = 0;
		double totalCost = 0;
		
		//Scanner to read the length of stay
		Scanner sc = new Scanner(System.in);
		
		//Display to the user
		System.out.println("== Caravan Park Charges ==");
		System.out.println("--------------------------");
		System.out.println("Are you a caravan park member? (Yes/No)");
		memberInput = sc.next();
		//Checking the input and setting the boolean variable
		if(memberInput.toLowerCase().startsWith("y")) {
			
			memberStatus = true;
			
		}
		else if(memberInput.toLowerCase().startsWith("n")) {
			
			memberStatus = false;
			
		}
		else {
			
			System.out.println("I am sorry, that is not a valid option, please try again");
			caravanCharges();
			
		}
		
		//Reading the input for the length of stay
		System.out.println("How long are you staying? (Please enter number of nights eg. 3)");
		lengthOfStay = sc.nextInt();
		
		//Working out and displaying the total cost of stay
		System.out.println("== Total Cost of Stay ==");
		if(memberStatus == true && lengthOfStay <= 7) {
			
			totalCost = lengthOfStay * 12.99;
			System.out.println("�"+totalCost);
			
		}
		else if(memberStatus == true && lengthOfStay > 7) {
			
			totalCost = lengthOfStay * 9.99;
			System.out.println("�"+totalCost);
			
		}
		else if(memberStatus == false && lengthOfStay <= 7) {
			
			totalCost = lengthOfStay * 14.99;
			System.out.println("�"+totalCost);
			
		}
		else if(memberStatus == false && lengthOfStay > 7) {
			
			totalCost = lengthOfStay * 11.99;
			System.out.println("�"+totalCost);
			
		}
		else {
			
			System.out.println("Something has went wrong, please try again.");
			caravanCharges();
			
		}
		
		System.out.println("Would you like to return to the main menu? (Yes/No)");
		menuChoice = sc.next();
		
		if(menuChoice.toLowerCase().startsWith("y")) {
			
			projectinterface.MainMenu.MainMenuInterface();
			
		}
		else if(menuChoice.toLowerCase().startsWith("n")) {
			
			System.out.println("Thank you, the program is now closing");
			System.exit(0);
			
		}
		else {
			
			System.out.println("I am sorry, that is not a valid option, please try again");
			caravanCharges();
			
		}
		
		//Closing the scanner to stop resource leaks
		sc.close();
		
	}

}
