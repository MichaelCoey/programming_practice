package projectclasses;

import java.util.Scanner;

public class Delivery {
	
	/* This class will calculate the delivery charge based on the product price and the distance
	 * that is required to deliver it. */
	
	public static void deliveryCharge() {
		
		//Variables
		double worth = 0.0f;
		double deliveryDistance = 0.0f;
		String expressChoice = "";
		boolean expressDelivery = false;
		
		
		//Scanner to read user input
		Scanner sc = new Scanner(System.in);
		
		//Output to user to get required information
		System.out.println("Please enter the products value. eg 2400 = �2400");
		worth = sc.nextDouble();
		
		System.out.println("Please enter the delivery distance in miles. eg 60");
		deliveryDistance = sc.nextDouble();
		
		System.out.println("Do you require express delivery? (Please type: Yes/No below)");
		expressChoice = sc.next();
		try{
			if(expressChoice.toLowerCase().startsWith("y")) {
				expressDelivery = true;
			}
			else if(expressChoice.toLowerCase().startsWith("n")) {
				expressDelivery = false;
			}
			else {
				System.out.println("Something has went wrong, please try again.");
				deliveryCharge();
			}
		}
		catch(Exception ex){
			System.out.println("Something has went wrong, please try again: " + ex.toString());
			deliveryCharge();
		}
		

		
		
		
		
		
		
		sc.close();
	}

}
