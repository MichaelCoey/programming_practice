package projectinterface;

import java.util.Scanner;

public class Main {
	
	/* This program will take five numbers that the user will enter when asked,
	 * it will then work out the average of the numbers that have been entered */

	public static void main(String[] args) {
		
		//Variables to store numbers in as well as count the numbers that the user enters
		double number1, number2, number3, number4, number5;
		double average;
		int numberCount = 0;
		
		//Scanner to read inputs
		Scanner sc = new Scanner(System.in);
		
		//Asking the user for the numbers
		System.out.println("Please enter the first number below:");
		number1 = sc.nextDouble();
		numberCount++;
		
		System.out.println("Please enter the second number below:");
		number2 = sc.nextDouble();
		numberCount++;
		
		System.out.println("Please enter the third number below:");
		number3 = sc.nextDouble();
		numberCount++;
		
		System.out.println("Please enter the fourth number below:");
		number4 = sc.nextDouble();
		numberCount++;
		
		System.out.println("Please enter the fifth number below:");
		number5 = sc.nextDouble();
		numberCount++;
		
		//Calculation to work out the average of the numbers
		average = (number1 + number2 + number3 + number4 + number5) / numberCount;
		
		//Output the average to the user
		System.out.println("The average of the numbers is: " + average );
		
		//Stopping resource leak
		sc.close();

	}

}
