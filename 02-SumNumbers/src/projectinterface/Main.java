package projectinterface;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//* Simple program to sum two numbers together that the user inputs *\\
		
		//Variables to store numbers
		double numberOne;
		double numberTwo;
		
		//Scanner to read user inputs
		Scanner sc = new Scanner(System.in);
		
		//Reading user input for first number
		System.out.println("Please enter the first number");
		numberOne = sc.nextDouble();
		
		//Reading user input for second number
		System.out.println("Please enter the second number");
		numberTwo = sc.nextDouble();
		
		//Sum to work out the sum of both numbers
		double totalNumber = numberOne + numberTwo;
		
		//Displaying the results to the user
		System.out.println("The sum of the two numbers is: ");
		System.out.println(numberOne + " + " + numberTwo + " = " + totalNumber);
		
		sc.close();

	}

}
