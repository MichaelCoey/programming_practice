package projectinterface;

import java.util.Scanner;

public class Main {
	
	/* The purpose of this program is to reverse a String input the the user enters and display it
	 * to the console when completed
	 * 
	 * == Test Data ==
	 * Input: Hello
	 * Output: olleH
	 *  */

	public static void main(String[] args) {
		
		//Two variables to store the Input and the Output
		String input;
		String output = "";
		
		//Scanner to read the user inputs
		Scanner sc = new Scanner(System.in);
		
		//Asking for the user input
		System.out.println("Please enter an input to reverse");
		input = sc.nextLine();
		
		//For loop to reverse the String input and store it in the output string
		for(int i = input.length() - 1; i >= 0; i--) {
			
			output = output + input.charAt(i);
				
		}
		
		//Output of the reversed input
		System.out.println(output);
		
		//Closing scanner to stop resource leak
		sc.close();

	}

}
